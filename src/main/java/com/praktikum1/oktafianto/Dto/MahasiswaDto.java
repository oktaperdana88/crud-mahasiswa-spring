package com.praktikum1.oktafianto.Dto;

import jakarta.validation.constraints.*;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class MahasiswaDto {
    private Long id;
    @NotNull(message = "NIM harus ada")
    @Pattern(regexp = "[0-9]+", message = "NIM tidak valid")
    @Size(min = 9, max = 9, message = "NIM terdiri dari 9 digit")
    private String NIM;
    @NotNull(message = "Nama tidak boleh kosong")
    @Pattern(regexp = "^[\\p{L}\\s.’\\-,]+$", message = "Nama tidak valid")
    private String nama;
    @NotNull(message = "Jurusan tidak boleh kosong")
    private String jurusan;
    @NotNull(message = "Tanggal lahir tidak boleh kosong")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date TTL;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date createdAt;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date updateAt;
}

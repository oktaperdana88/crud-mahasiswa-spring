package com.praktikum1.oktafianto.Exception;

public class MahasiswaNotFoundException extends Exception{
    public MahasiswaNotFoundException(String message) {
        super(message);
    }
}

package com.praktikum1.oktafianto.Exception;

public class MahasiswaHasBeenRegistered extends Exception{
    public MahasiswaHasBeenRegistered(String message) {
        super(message);
    }
}

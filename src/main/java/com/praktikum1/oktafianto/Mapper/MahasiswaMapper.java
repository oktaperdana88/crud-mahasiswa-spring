package com.praktikum1.oktafianto.Mapper;

import com.praktikum1.oktafianto.Dto.MahasiswaDto;
import com.praktikum1.oktafianto.Entity.Mahasiswa;

public class MahasiswaMapper {
    public static MahasiswaDto modelToDto(Mahasiswa mhs) {
        return MahasiswaDto.builder()
                .id(mhs.getId())
                .NIM(mhs.getNIM())
                .nama(mhs.getNama())
                .jurusan(mhs.getJurusan())
                .TTL(mhs.getTTL())
                .createdAt(mhs.getCreatedAt())
                .updateAt(mhs.getUpdateAt())
                .build();
    }

    public static Mahasiswa dtoToModel(MahasiswaDto mahasiswaDto) {
        return Mahasiswa.builder()
                .id(mahasiswaDto.getId())
                .NIM(mahasiswaDto.getNIM())
                .nama(mahasiswaDto.getNama())
                .jurusan(mahasiswaDto.getJurusan())
                .TTL(mahasiswaDto.getTTL())
                .createdAt(mahasiswaDto.getCreatedAt())
                .updateAt(mahasiswaDto.getUpdateAt())
                .build();
    }
}

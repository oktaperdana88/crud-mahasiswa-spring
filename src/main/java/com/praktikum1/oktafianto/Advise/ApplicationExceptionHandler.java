package com.praktikum1.oktafianto.Advise;

import com.praktikum1.oktafianto.Exception.MahasiswaHasBeenRegistered;
import com.praktikum1.oktafianto.Exception.MahasiswaNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class ApplicationExceptionHandler {
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleArgumentValidationException(MethodArgumentNotValidException e) {
        Map<String, String> errorMap = new HashMap<>();
        e.getBindingResult().getFieldErrors().forEach(error -> {
            errorMap.put(error.getField(), error.getDefaultMessage());
        });
        return errorMap;
    }

    @ExceptionHandler(MahasiswaNotFoundException.class)
    public ResponseEntity<Object> handleMahasiswaNotFoundException(MahasiswaNotFoundException e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(MahasiswaHasBeenRegistered.class)
    public ResponseEntity<Object> handleMahasiswaHasBeenRegisteredException(MahasiswaHasBeenRegistered e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
    }
}

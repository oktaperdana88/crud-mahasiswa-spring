package com.praktikum1.oktafianto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OktafiantoApplication {

	public static void main(String[] args) {
		SpringApplication.run(OktafiantoApplication.class, args);
	}

}

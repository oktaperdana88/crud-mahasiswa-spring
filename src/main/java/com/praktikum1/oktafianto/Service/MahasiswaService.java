package com.praktikum1.oktafianto.Service;

import com.praktikum1.oktafianto.Dto.MahasiswaDto;
import com.praktikum1.oktafianto.Exception.MahasiswaNotFoundException;

import java.util.List;

public interface MahasiswaService {
    public List<MahasiswaDto> getAllMahasiswa();
    public List<MahasiswaDto> getMahasiswaByNIMOrName(String search) throws MahasiswaNotFoundException;
    public MahasiswaDto getMahasiswaByNIM(String NIM);
    public void createMahasiswa(MahasiswaDto mahasiswaDto) throws MahasiswaNotFoundException;
    public void updateMahasiswa(Long id, MahasiswaDto mahasiswaDto) throws MahasiswaNotFoundException;
    public void deleteMahasiswa(String NIM);
}

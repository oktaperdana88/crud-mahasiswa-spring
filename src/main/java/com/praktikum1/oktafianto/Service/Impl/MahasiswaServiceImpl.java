package com.praktikum1.oktafianto.Service.Impl;

import com.praktikum1.oktafianto.Dto.MahasiswaDto;
import com.praktikum1.oktafianto.Exception.MahasiswaNotFoundException;
import com.praktikum1.oktafianto.Mapper.MahasiswaMapper;
import com.praktikum1.oktafianto.Entity.Mahasiswa;
import com.praktikum1.oktafianto.Repository.MahasiswaRepository;
import com.praktikum1.oktafianto.Service.MahasiswaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MahasiswaServiceImpl implements MahasiswaService {

    @Autowired
    private MahasiswaRepository mahasiswaRepository;
    @Override
    public List<MahasiswaDto> getAllMahasiswa() {
        List<Mahasiswa> mahasiswas = mahasiswaRepository.findAll();
        List<MahasiswaDto> mahasiswaDtos = mahasiswas.stream()
                .map(MahasiswaMapper::modelToDto)
                .collect(Collectors.toList());
        return mahasiswaDtos;
    }

    @Override
    public List<MahasiswaDto> getMahasiswaByNIMOrName(String search) throws MahasiswaNotFoundException {
        List<Mahasiswa> mahasiswa = mahasiswaRepository.findAllByNIMOrNama(search);
        if (mahasiswa != null) {
            List<MahasiswaDto> mahasiswaDtos = mahasiswa.stream()
                    .map(MahasiswaMapper::modelToDto)
                    .collect(Collectors.toList());
            return mahasiswaDtos;
        } else {
            throw new MahasiswaNotFoundException("Mahasiswa dengan NIM atau nama "+search+" tidak ditemukan!");
        }
    }

    @Override
    public void createMahasiswa(MahasiswaDto mahasiswaDto) throws MahasiswaNotFoundException {
        Mahasiswa mahasiswa = MahasiswaMapper.dtoToModel(mahasiswaDto);
        Optional<Mahasiswa> mahasiswaOptional = mahasiswaRepository.findByNIM(mahasiswa.getNIM());
        if (mahasiswaOptional.isPresent()) {
            throw new MahasiswaNotFoundException("NIM telah terdaftar");
        }
        mahasiswaRepository.save(mahasiswa);
    }

    @Override
    public void updateMahasiswa(Long id, MahasiswaDto mahasiswaDto) throws MahasiswaNotFoundException {
        Optional<Mahasiswa> mahasiswa = mahasiswaRepository.findById(id);
        Optional<Mahasiswa> mahasiswaOptional = mahasiswaRepository.findByNIM(mahasiswaDto.getNIM());
        if (mahasiswaOptional.isPresent()) {
            Mahasiswa mahasiswa1 = mahasiswaOptional.get();
            if (!mahasiswa1.getId().equals(id)) {
                throw new MahasiswaNotFoundException("NIM telah didaftarkan oleh mahasiswa lain");
            }
        }
        if (mahasiswa.isPresent()) {
            Mahasiswa mahasiswa1 = mahasiswa.get();
            mahasiswa1.setNIM(mahasiswaDto.getNIM());
            mahasiswa1.setNama(mahasiswaDto.getNama());
            mahasiswa1.setJurusan(mahasiswaDto.getJurusan());
            mahasiswa1.setTTL(mahasiswaDto.getTTL());
            mahasiswaRepository.save(mahasiswa1);
        } else {
            throw new MahasiswaNotFoundException("Mahasiswa tidak ditemukan!");
        }
    }

    @Override
    public MahasiswaDto getMahasiswaByNIM (String NIM) {
        Optional<Mahasiswa> mahasiswa = mahasiswaRepository.findByNIM(NIM);
        if(mahasiswa.isPresent()) {
            Mahasiswa mahasiswa1 = mahasiswa.get();
            MahasiswaDto mahasiswaDto = MahasiswaDto.builder()
                    .id(mahasiswa1.getId())
                    .NIM(mahasiswa1.getNIM())
                    .nama(mahasiswa1.getNama())
                    .TTL(mahasiswa1.getTTL())
                    .jurusan(mahasiswa1.getJurusan())
                    .updateAt(mahasiswa1.getUpdateAt())
                    .createdAt(mahasiswa1.getCreatedAt())
                    .build();
            return mahasiswaDto;
        }
        return null;
    }

    @Override
    public void deleteMahasiswa(String NIM) {
        mahasiswaRepository.deleteByNIM(NIM);
    }
}

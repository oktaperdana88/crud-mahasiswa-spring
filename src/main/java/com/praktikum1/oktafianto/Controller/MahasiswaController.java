package com.praktikum1.oktafianto.Controller;

import com.praktikum1.oktafianto.Dto.MahasiswaDto;
import com.praktikum1.oktafianto.Exception.MahasiswaNotFoundException;
import com.praktikum1.oktafianto.Service.MahasiswaService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
public class MahasiswaController {
    @Autowired
    private MahasiswaService mahasiswaService;

    private static List<String> jurusan = new ArrayList<>();
    static {
        jurusan.add("D-3 Statistika");
        jurusan.add("D-4 Statistika");
        jurusan.add("D-4 Komputasi Statistik");
    }

    @GetMapping("/")
    public String home (Model model) {
        List<MahasiswaDto> mahasiswaDtos = mahasiswaService.getAllMahasiswa();
        model.addAttribute("mahasiswas", mahasiswaDtos);
        return "index";
    }

    @GetMapping("/search")
    public String search (Model model, @RequestParam String query) throws MahasiswaNotFoundException {
        System.out.println(query);
        model.addAttribute("mahasiswas",mahasiswaService.getMahasiswaByNIMOrName(query));
        return "index";
    }

    @GetMapping("/view/{NIM}")
    public String view (@PathVariable(name = "NIM") String NIM, Model model) throws MahasiswaNotFoundException {
        MahasiswaDto mahasiswaDto = mahasiswaService.getMahasiswaByNIM(NIM);
        model.addAttribute("mahasiswa",mahasiswaDto);
        return "view";
    }

    @GetMapping("/edit/{NIM}")
    public String Edit (@PathVariable(name = "NIM") String NIM, Model model) {
        MahasiswaDto mahasiswaDto = mahasiswaService.getMahasiswaByNIM(NIM);
        model.addAttribute("mahasiswa",mahasiswaDto);
        model.addAttribute("jurusan", jurusan);
        return "edit";
    }

    @GetMapping("/create")
    public String create (Model model) {
        model.addAttribute("jurusan", jurusan);
        return "create";
    }

    @PostMapping("/create")
    public String createPost (@Valid MahasiswaDto mahasiswa) throws MahasiswaNotFoundException {
        mahasiswaService.createMahasiswa(mahasiswa);
        return "redirect:/";
    }

    @PostMapping("/edit/{id}")
    public String editPost (@ModelAttribute MahasiswaDto mahasiswa, @PathVariable(name = "id") Long id) throws MahasiswaNotFoundException {
        mahasiswaService.updateMahasiswa(id, mahasiswa);
        return "redirect:/";
    }

    @GetMapping("/delete/{NIM}")
    public String delete (@PathVariable (name = "NIM") String NIM) {
        mahasiswaService.deleteMahasiswa(NIM);
        return "redirect:/";
    }
}

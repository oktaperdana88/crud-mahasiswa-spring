package com.praktikum1.oktafianto.Repository;

import com.praktikum1.oktafianto.Entity.Mahasiswa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface MahasiswaRepository extends JpaRepository<Mahasiswa, Long> {
    @Query("SELECT m FROM Mahasiswa m WHERE m.nama LIKE concat('%',?1,'%') OR m.NIM LIKE concat('%',?1,'%')")
    List<Mahasiswa> findAllByNIMOrNama(String query);

    @Query("SELECT m FROM Mahasiswa m WHERE m.NIM = ?1")
    Optional<Mahasiswa> findByNIM(String NIM);

    @Modifying
    @Transactional
    @Query("DELETE FROM Mahasiswa m WHERE m.NIM = ?1")
    void deleteByNIM(String NIM);
}
